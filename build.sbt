name := "recopick-spark"

version := "1.0"

scalaVersion := "2.11.8"

val sparkGroupId = "org.apache.spark"
val sparkVersion = "2.0.0"
val sparkScope = "compile"
//val sparkScope = "provided"

libraryDependencies += sparkGroupId % "spark-core_2.11" % sparkVersion % sparkScope exclude("org.scalatest", "scalatest_2.11")
libraryDependencies += sparkGroupId % "spark-streaming_2.11" % sparkVersion % sparkScope exclude("org.scalatest", "scalatest_2.11")
libraryDependencies += sparkGroupId % "spark-streaming-kafka-0-8_2.11" % sparkVersion % "provided" exclude("org.scalatest", "scalatest_2.11")
libraryDependencies += sparkGroupId % "spark-sql_2.11" % sparkVersion % sparkScope exclude("org.scalatest", "scalatest_2.11")
libraryDependencies += sparkGroupId % "spark-hive_2.11" % sparkVersion % sparkScope exclude("org.scalatest", "scalatest_2.11")


libraryDependencies += "org.apache.hadoop" % "hadoop-aws" % "2.7.3"
libraryDependencies += "com.google.code.gson" % "gson" % "2.7"
libraryDependencies += "org.json4s" % "json4s-native_2.11" % "3.4.0"

// mysql
libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.39"

// for URL parsing
libraryDependencies += "com.squareup.okhttp" % "okhttp" % "2.7.5"

// test
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.0"
libraryDependencies += "org.scalatest" % "scalatest_2.11" % "3.0.0" % "test"
libraryDependencies += "junit" % "junit" % "4.12" % "test"

assemblyJarName in assembly := "recopick-spark-assembly.jar"
//mainClass in assembly := Some("driver.streaming.DirectKafkaInput")

assemblyOption in assembly :=
  (assemblyOption in assembly).value.copy(includeScala = false)

assemblyMergeStrategy in assembly := {
  case m if m.toLowerCase.endsWith("manifest.mf") => MergeStrategy.discard
  case m if m.startsWith("META-INF") => MergeStrategy.discard
  case PathList("org", "apache", xs @ _*) => MergeStrategy.first
  case _ => MergeStrategy.first
}


    