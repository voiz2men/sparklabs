package com.skplanet.recopick.spark.driver

import org.apache.spark.{SparkConf, SparkContext}

// raw 로그를 기반으로 서비스 ID별 호출갯수 구하기
object CountActionLogByServiceID {
  def main(args: Array[String]): Unit = {

    val counter = 0
    val sparkConf = new SparkConf().setAppName("My Spark Job").setMaster("local")
    val sc = new SparkContext(sparkConf)

    //val input = sc.textFile("file:///Users/1001948/Documents/recopick/log/2016-04-04_013000_clicklog.txt")
    val input = sc.textFile("src/main/resources/sample/log/20170611.txt")

    val pairsByServiceID = input.map( x => (x.split("\t")(0), x))
    val countByKey = pairsByServiceID.countByKey()


    countByKey.foreach(println)

  }
}
