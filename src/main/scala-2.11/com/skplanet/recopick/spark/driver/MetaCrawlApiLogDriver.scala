package com.skplanet.recopick.spark.driver

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}

object MetaCrawlApiLogDriver {

  /**
    * @see http://www.cloudera.com/documentation/enterprise/5-5-x/topics/spark_s3.html
    * @param sc SparkContext
    */
  def s3Configuration(sc: SparkContext): Unit = {
    sc.hadoopConfiguration.set("fs.s3n.impl", "org.apache.hadoop.fs.s3native.NativeS3FileSystem")
    sc.hadoopConfiguration.set("fs.s3n.awsAccessKeyId", "AKIAJNK7PSZD52WVX2KA")
    sc.hadoopConfiguration.set("fs.s3n.awsSecretAccessKey", "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")
  }

  val serviceIdStr = "serviceId : "
  val itemIdStr = ", itemId : "
  val fromStr = ", from : "

  /**
    *
    * @param args
    * s3 input path example : s3n://recopick-meta-crawl-api-log/log/2016/6/15/10/spring.log.1-ip-10-166-20-240-10.166.20.240
    * local input path example : ./src/main/resources/sample/metacrawl-api-log-20160825.samplelog
    * s3 output path example : s3n://recopick-meta-crawl-api-log/result/20160615
    * local output path example : ./src/main/resources/result
    */
  def main(args: Array[String]): Unit = {

    val inputPath = Option(args(0)) match {
      case Some(s) if !s.isEmpty => s
      case _ => throw new IllegalArgumentException("inputPath can not be null or empty")
    }
    val outputPath = Option(args(1)) match {
      case Some(s) if !s.isEmpty => s
      case _ => throw new IllegalArgumentException("outputPath can not be null or empty")
    }

    val conf = new SparkConf().setMaster("local").setAppName("MetaCrawlApiLogS3Driver")
    val sc = new SparkContext(conf)
    s3Configuration(sc)

    val fullLogs = sc.textFile(inputPath)

    val collectRequests = fullLogs.filter(line => line.contains("ItemCollectRequest"))

    val transformed = collectRequests.map(line => {
      val time = line.substring(0, 23)

      val serviceIdStartIndex = line.indexOf(serviceIdStr)
      val itemIdStartIndex = line.indexOf(itemIdStr)
      val itemIdEndIndex = line.indexOf(fromStr)

      val serviceId = line.substring(serviceIdStartIndex + serviceIdStr.length, itemIdStartIndex)
      val itemId = line.substring(itemIdStartIndex + itemIdStr.length, itemIdEndIndex)

      val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")
      val dateTime = LocalDateTime.parse(time, formatter)
      val kstDateTime = dateTime.plusHours(9L)

      val month = kstDateTime.getMonthValue
      val dayOfMonth = kstDateTime.getDayOfMonth
      val hour = kstDateTime.getHour

      (month, dayOfMonth, hour, serviceId, itemId)
    })

    transformed.persist(StorageLevel.DISK_ONLY)


    val total = transformed.map(t => (t._1 + "/" + t._2 + ":" + t._3, 1)).reduceByKey(_ + _)
    val distinct = transformed.distinct().map(t => (t._1 + "/" + t._2 + ":" + t._3, 1)).reduceByKey(_ + _)

    val cogroup = total.cogroup(distinct)

    val sorted = cogroup.collect().map(t => {
      val monthEndIdx = t._1.indexOf('/')
      val dayEndIdx = t._1.indexOf(':')
      val month = t._1.substring(0, monthEndIdx).toInt
      val day = t._1.substring(monthEndIdx + 1, dayEndIdx).toInt
      val hour = t._1.substring(dayEndIdx + 1, t._1.length).toInt

      val totalArr = t._2._1.toArray
      val total = totalArr(0)

      val distinctArr = t._2._2.toArray
      val distinct = distinctArr(0)

      (month, day, hour, total, distinct, (distinct * 100.0 / total + 0.5).toInt)
    }).sortWith((t1, t2) => {
      val t1Month = t1._1
      val t1Day = t1._2
      val t1Hour = t1._3

      val t2Month = t2._1
      val t2Day = t2._2
      val t2Hour = t2._3

      if (t1Month != t2Month) {
        t1Month < t2Month
      }
      else if (t1Day != t2Day) {
        t1Day < t2Day
      }
      else t1Hour < t2Hour
    })
      .map(tuple => tuple.productIterator.mkString(","))

    val result = sc.parallelize(sorted).repartition(1)
    result.saveAsTextFile(outputPath)

    transformed.unpersist()

    sc.stop()
  }

}
