package com.skplanet.recopick.spark.driver

import java.io.UnsupportedEncodingException
import java.net.{URI, URLDecoder}
import java.time.{Instant, ZoneId, ZonedDateTime}
import java.time.format.DateTimeFormatter
import java.util.regex.{Matcher, Pattern}

import com.squareup.okhttp.HttpUrl
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}
import org.json4s._
import org.json4s.native.JsonMethods._

/**
  * original : coruscant/RealTimeUserActionCounter
  *
  */
object RealTimeUserActionCounterBatchDriver {
  
  // for json4s
  implicit val formats = DefaultFormats

  def main(args: Array[String]): Unit = {
    if (args.size != 1) {
      println("Usage : <input path>")
      return
    }

    val inputPath = Option(args(0)) match {
      case Some(s) if (!s.isEmpty) => s
      case _ => throw new IllegalArgumentException("inputPath can not be null or empty")
    }

    val conf = new SparkConf().setMaster("local").setAppName("RealTimeUserActionCounterDriver")
    val sc = new SparkContext(conf)

    val fullLogs = sc.textFile(inputPath)

    val json = fullLogs
      // map to JValue
      .map(str => parse(str))
      // extract values (SID, TIMESTAMP(yyyyMMddHH), URL, REFERRER)
      .map(json => {

        val sid = json findField (field => field._1.equalsIgnoreCase("sid")) match {
          case Some(s) => s._2.extract[String]
          case _ => null
        }

        // NOTE : ts can not be null or empty. @see LogCollector
        val ts = json findField (field => field._1.equalsIgnoreCase("ts")) match {
          case Some(s) => s._2.extract[String]
          case _ => null
        }

        var url = json findField (field => field._1.equalsIgnoreCase("url")) match {
          case Some(s) => s._2.extract[String]
          case _ => null
        }

        var tag = json findField (field => field._1.equalsIgnoreCase("tag")) match {
          case Some(s) => s._2.extract[String]
          case _ => null
        }

        url = tryDecode(url)
        tag = normalizeTag(tag)

        // yyyyMMddHH
        val dateTime = DateTimeFormatter.ofPattern("yyyyMMddHH")
          .format(ZonedDateTime.ofInstant(Instant.ofEpochMilli(ts.toLong), ZoneId.of("UTC")))

        (sid, dateTime, url, tag)
      })
      // url null or empty filter
      .filter(t => t._3 != null && !t._3.isEmpty)
      // parsing tag & extract actionCode
      .map(t => {
        val tagJson = parse(t._4)

        val actionAlgorithm = tagJson findField (field => field._1.equalsIgnoreCase("action")) match {
          case Some(s) => s._2.extract[String]
          case None => null
        }

        val recoclick = tagJson findField (f => f._1.equalsIgnoreCase("recoclick")) match {
          case Some(s) => s._2.extract[Int]
          // default value
          case None => 0
        }

        var actionAlgorithmCode = actionAlgorithm match {
          case "view" => 1
          case "order" => 101
          case "basket" => 201
          case "search" => 301
          case "like" => 401
          // default value
          case _ => 0
        }

        if (recoclick > 0) {
          actionAlgorithmCode += recoclick - 1
        }

        val sid = t._1
        val dateTime = t._2
        val url = t._3
        actionAlgorithmCode = correctActionAlgorithm(url, actionAlgorithmCode)

        (sid, dateTime, actionAlgorithmCode)
      })

    json.persist(StorageLevel.DISK_ONLY)

    // action count
    val actionCountRDD = json.countByValue()

    // pageView
    val pageViewRDD = json
      .map(t => {
        (t._1, t._2)
      })
      .countByValue()

    // NOTE : for test
    actionCountRDD.foreach(entry => {
      val sid = entry._1._1
      val dateTime = entry._1._2
      val actionCode = entry._1._3
      val count = entry._2

      println(sid + "," + dateTime + "," + actionCode + "," + count)
    })

    pageViewRDD.foreach(entry => {
        println(entry._1._1 + "," + entry._1._2 + "," + entry._2)
    })

    // TODO save pv to DB (recopick_admin.HOURLY_PV_UV)
    // INSERT INTO recopick_admin.HOURLY_PV_UV (`service_id`, `hour`, `count`) values (?,?,?) on duplicate key update count = count + ?

    // TODO save actionCount to DB (recopick_admin.HOURLY_ACTION)
    // INSERT INTO recopick_admin.HOURLY_ACTION (`service_id`, `action_id`, `hour`, `count`) values (?,?,?,?) on duplicate key update count = count + ?


    json.unpersist()
  }

  /**
    * 추천을 클릭한 상황에 제대로 actionCode 에 반영되지 않은 경우 actionCode 에 알고리즘을 반영
    *
    * @param url
    * @param actionCode
    * @return
    */
  def correctActionAlgorithm(url: String, actionCode: Int): Int = {
    try {
      val recoAlgorithm = HttpUrl.parse(url).queryParameter("recopick")
      //url에 오류가 있을 경우, error log가 많이 떨어지므로, 오류가 있는 경우 아예 시도하지 말자
      URI.create(url)
      if (recoAlgorithm != null && (actionCode % 100 == 1)) {
        return actionCode + recoAlgorithm.toInt - 1
      }
    }
    catch {
      case e: Throwable => {
        // ignore
      }
    }
    return actionCode
  }

  def tryDecode(url: String): String = {
    var urlVar = url
    try {
      urlVar = decodeUrlIfPossible(urlVar)
      urlVar = removeIllegalValInUrl(urlVar)
    }
    catch {
      case e: Exception => {
        // ignore
        return url
      }
    }
    urlVar
  }

  private val PercentWithOneDigit: Pattern = Pattern.compile("%[\\d]?$")

  // 11번가는 url을 두 번 encoding해서 주는 문제가 있기 때문에, 특정 패턴으로 시작될 경우 decode.
  private def decodeUrlIfPossible(url: String): String = {
    //맨 끝에 % 혹은 %숫자가 나오는 경우는 오류임
    val m: Matcher = PercentWithOneDigit.matcher(url)
    if (m.find) {
      val wrongPart: String = m.group(0)
      return url.substring(0, url.length - wrongPart.length)
    }
    if (url.startsWith("http%3A%2F%2F") || url.startsWith("https%3A%2F%2F")) {
      try
        return URLDecoder.decode(url, "EUC_KR")
      catch {
        case e: UnsupportedEncodingException => {
          // ignore
          return url
        }
      }
    }
    url
  }

  // 허용되지 않는 문자 제거
  def removeIllegalValInUrl(url: String): String = {
    var urlVar = url
    urlVar = urlVar.replaceAll(" ", "%20")
    urlVar = urlVar.replaceAll("\\|", "%7C")
    urlVar = urlVar.replaceAll("\\^", "%5E")
    urlVar = urlVar.replaceAll("\t", "")
    urlVar
  }

  def normalizeTag(tag: String): String = {
    if (tag == null || tag.trim.length == 0)
      "\\N"
    else
      tag.replaceAll("\t", " ").replaceAll("\n", " ")
  }


}
