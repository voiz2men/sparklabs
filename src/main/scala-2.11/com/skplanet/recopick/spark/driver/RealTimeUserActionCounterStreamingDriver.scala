package com.skplanet.recopick.spark.driver

import kafka.serializer.StringDecoder
import org.apache.spark.SparkConf
import org.apache.spark.streaming.kafka.KafkaUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}

object RealTimeUserActionCounterStreamingDriver {

  def main(args: Array[String]): Unit = {
    val topics = Set[String]("etl-raw-log-service")
    val kafkaParams = scala.collection.immutable.Map[String, String]("metadata.broker.list" ->
      "kafka1.recopick.com:9092,kafka2.recopick.com:9092,kafka3.recopick.com:9092,kafka4.recopick.com:9092")

    val conf = new SparkConf().setMaster("local").setAppName("RealTimeUserActionCounter")
    val ssc = new StreamingContext(conf, Seconds(10L))
    val directKafkaStream = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](ssc, kafkaParams, topics)
    // total attributes : sid, timestamp, url, ref, tag


    // count pageView By sid, timestamp(yyyyMMddHH)
    // TODO : save to DB using batchUpdate


    // actionId by tag, url, ref


    // count action by sid, aid, timestamp(yyyyMMddHH)

    // save to DB
    // batchUpdate

    ssc.start()
    // Wait for 10 seconds then exit. To run forever call without a timeout
    ssc.awaitTerminationOrTimeout(15000L)
    ssc.stop()
  }


}
