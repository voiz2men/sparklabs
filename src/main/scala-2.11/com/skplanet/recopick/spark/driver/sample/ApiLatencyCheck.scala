package com.skplanet.recopick.spark.driver.sample

import org.apache.spark.{SparkConf, SparkContext}

// API 로그에서 특정 속도 이상의 API 찾기
object ApiLatencyCheck {
  val filePath = "src/main/resources/sample/log/api_server_log.txt"
  //val filePath = "src/main/resources/sample/log/2017-10-01_000000_clicklog.txt"

  def main(args: Array[String]): Unit = {
    var conf = new SparkConf().setMaster("local").setAppName("ApiLatencyCheck")
    var sc = new SparkContext(conf)

    try{
      val apiLogRDD = sc.textFile(filePath).map {
        record =>
          val splitRecord = record.split(" ")
          val latency = splitRecord(4).toFloat + splitRecord(5).toFloat + splitRecord(6).toFloat
          val statusCode = splitRecord(8)
          val calledApi = splitRecord(12)
          (statusCode, latency, calledApi)
      }

      // clickLogRDD.foreach(println)
      val checkHighLatencyRDD = apiLogRDD.filter(_._2 > 1.0)

      checkHighLatencyRDD.foreach(println)


    } finally {
      sc.stop()
    }
  }

}
