package com.skplanet.recopick.spark.driver.sample

import org.apache.spark.{SparkConf, SparkContext}

object BasicRDD1 {

  def main(args: Array[String]): Unit = {

    val conf = new SparkConf().setMaster("local").setAppName("단어 갯수구하기")
    val sc = new SparkContext(conf)

    val textRDD = sc.textFile("src/main/resources/sample/simple-words.txt")
    val textArray = textRDD.collect
    //textArray.foreach(println)

    // Filltering (대,소문자 알파벳숫자)
    // wordRDD1,2,3은 모두 동일한 결과를 가진다
    val isWord: String => Boolean = word => word.matches("""\p{Alnum}+""")
    val wordRDD1 = textRDD.filter(isWord)
    val wordArray = wordRDD1.collect
    val wordRDD2 = textRDD.filter(word => word.matches("""\p{Alnum}+"""))
    val wordRDD3 = textRDD.filter(_.matches("""\p{Alnum}+"""))

    // (단어, 1) 형의 튜플을 요소로 가진 RDD를 생성
    val wordAndOnePairRDD = wordRDD3.map(word => (word,1))
    val wordAndOnePairArray = wordAndOnePairRDD.collect()
    wordAndOnePairArray.foreach(println)

    // reduceByKey를 이용해 각 단어의 갯수를 구한다
    // 아래 3가지 모두 결과값 동일
    val wordAndCountRDD = wordAndOnePairRDD.reduceByKey((result, elem) => result + elem)
    wordAndCountRDD.foreach(println)

    val wordAndCountRDD2 = wordAndOnePairRDD.reduceByKey((result, elem) => result + 1)
    wordAndCountRDD2.foreach(println)

    val wordAndCountRDD3 = wordAndOnePairRDD.reduceByKey(_+_)
    wordAndCountRDD3.foreach(println)

    //println(wordAndCountRDD.count())


  }
}
