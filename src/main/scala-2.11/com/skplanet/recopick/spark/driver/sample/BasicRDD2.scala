package com.skplanet.recopick.spark.driver.sample

import org.apache.spark.{SparkConf, SparkContext}

object BasicRDD2 {

  def main(args: Array[String]): Unit = {

    val conf = new SparkConf().setMaster("local").setAppName("포함된 단어의 갯수 구하기")
    val sc = new SparkContext(conf)

    // Sample README.md 설정
    val filePath = "src/main/resources/sample/README.md"
    val textRDD = sc.textFile(filePath)

    // 포함된 단어의 갯수 구하기
    // 단어 추출
    val wordCandidateRDD = textRDD.flatMap(_.split("[ ,.]"))
    val wordCandidateArray = wordCandidateRDD.collect()
    wordCandidateArray.foreach(println)

    // 단어별 갯수구하기
    val wordRDD = wordCandidateRDD.filter(_.matches("""\p{Alnum}+"""))
    val wordAndOnePairRDD = wordRDD.map((_,1))
    val wordAndCountRDD = wordAndOnePairRDD.reduceByKey(_+_)
    wordAndCountRDD.foreach(println)

    // 위의 단계를 아래와 같이 만들수도 있음
    /*
    val wordAndCountRDD = sc.textFile(filePath)
                              .flatMap(_.split("[ ,.]"))
                              .filter(_.matches("""\p{Alnum}+"""))
                              .map((_,1))
                              .reduceByKey(_+_)
    */

    // 가장많이 포함된 3개의 단어 구하기
    val wordAndCountSortedRDD = wordAndCountRDD.map{
      case(word, count) => (count, word)
    }.sortByKey(false).map{
      case(count, word) => (word, count)
    }.take(3)

    wordAndCountSortedRDD.foreach(println)


  }
}
