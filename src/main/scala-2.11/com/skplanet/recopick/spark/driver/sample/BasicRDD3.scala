package com.skplanet.recopick.spark.driver.sample

import scala.collection.mutable.{HashMap, Map}
import java.io.{BufferedReader, InputStreamReader, Reader}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.rdd.RDD

object BasicRDD3 {

  val productsCSVFile = "src/main/resources/sample/products.csv"
  val octFilePath = "src/main/resources/sample/sales-october.csv"
  val novFilePath = "src/main/resources/sample/sales-november.csv"
  val outputPath = "src/main/resources/sample/output"

  /*
    판매실적 데이터가 기록되어 있는 파일로부터
    (상품ID, 판매개수)를 요소로 갖는 RDD를 생성하는 메소
   */
  private def createSalesRDD(csvFile: String, sc:SparkContext) = {
    val logRDD = sc.textFile(csvFile)
    logRDD.map {
      record =>
        val splitRecord = record.split(",")
        val productId = splitRecord(2)
        val numOfSold = splitRecord(3).toInt
        (productId, numOfSold)
    }
  }

  /*
    각 달의 판매실적 데이터를 갖고 있는 RDD로 부터
    50개이상 팔린 상품의 정보만을 갖는 RDD를  생성하는 메소드
   */
  private def createOver50SoldRDD(rdd: RDD[(String, Int)]) = {
    rdd.reduceByKey(_+_).filter(_._2 >= 50)
  }

  /*
    상품의 마스터 데이터를 갖는 HaspMap을 생성하는 메쏘드
   */
  private def loadCSVIntoMap(productsCSVFile: String) = {
    var productsCSVReader: Reader = null

    try{
      val productsMap = new HashMap[String, (String, Int)]
      val hadoopConf = new Configuration
      val fileSystem = FileSystem.get(hadoopConf)
      val inputStream = fileSystem.open(new Path(productsCSVFile))
      val productsCSVReader = new BufferedReader(new InputStreamReader(inputStream))
      var line = productsCSVReader.readLine

      while (line != null) {
        val splitLine = line.split(",")
        val productId = splitLine(0)
        val productName = splitLine(1)
        val unitPrice = splitLine(2).toInt
        productsMap(productId) = (productName, unitPrice)
        line = productsCSVReader.readLine
      }
      productsMap

    } finally {
      if(productsCSVReader != null){
        productsCSVReader.close()
      }
    }
  }

  /*
    상품의 마스터 데이터와 결합해서
    두달 연속으로 50개 이상 판매된 상품정보를 갖는 RDD를 생성하는 메소드   */
  private def createResultRDD( broadcastedMap: Broadcast[_ <: Map[String, (String, Int)]],
                               rdd: RDD[(String, Int)]) = {
    rdd.map {
      case (productId, amount) =>
        val productsMap = broadcastedMap.value
        val (productName, unitPrice) = productsMap(productId)
        (productName, amount, amount * unitPrice)
    }
  }

  def main(args: Array[String]): Unit = {

    val conf = new SparkConf().setMaster("local").setAppName("BasicRDD3")
    val sc = new SparkContext(conf)

    try {
      val salesOctRdd = createSalesRDD(octFilePath, sc)
      val salesNovRdd = createSalesRDD(novFilePath, sc)

      // 50개 이상 팔린 상품 찾기
      val octOver50SoldRDD = createOver50SoldRDD(salesOctRdd);
      val novOver50SoldRDD = createOver50SoldRDD(salesNovRdd);
      //octOver50SoldRDD.foreach(println)
      //novOver50SoldRDD.foreach(println)

      // 10월과 11월 모두 50개 이상 팔린상품 찾기
      val bothOver50SoldRDD = octOver50SoldRDD.join(novOver50SoldRDD)
      //bothOver50SoldRDD.collect().foreach(println)

      // (상품ID, 두달간의 총판매개수)형식의 튜플을 요소로 갖는 RDD를 생성한다
      val over50SoldAndAmountRDD = bothOver50SoldRDD.map {
        case (productId, (amount1, amount2)) =>
          (productId, amount1 + amount2)
      }
      //over50SoldAndAmountRDD.collect().foreach(println)

      // 상품의 마스터 데이터를 HashMap에 로드하고
      // 브로드캐스트 변수로 이그제큐터에 배포한다
      val productsMap = loadCSVIntoMap(productsCSVFile)
      val broadcastedMap = sc.broadcast(productsMap)

      // 결과를 계산해 파일시스템에 출력한다
      val resultRDD = createResultRDD(broadcastedMap, over50SoldAndAmountRDD)
      //resultRDD.saveAsTextFile(outputPath)
      resultRDD.collect().foreach(println)

    } finally {

      sc.stop()

    }

  }

}
