package com.skplanet.recopick.spark.driver.sample

import org.apache.spark.{SparkConf, SparkContext}

// Recommend List가 없는 Click 로그 찾기
object ClickLogVerify {
  val filePath = "src/main/resources/sample/log/2017-10-01_000000_clicklog.txt"

  def main(args: Array[String]): Unit = {
    var conf = new SparkConf().setMaster("local").setAppName("ClickLogVerify")
    var sc = new SparkContext(conf)

    try{
      val clickLogRDD = sc.textFile(filePath).map {
        record =>
          val splitRecord = record.split("\t")
          val serviceId = splitRecord(2)
          val recommendListLength = splitRecord(5).length
          (serviceId, recommendListLength)
      }

      // clickLogRDD.foreach(println)
      val filteredClickLogRDD = clickLogRDD.filter(_._2 == 0)
      val recommendListRDD = filteredClickLogRDD.map((_,1))
      val zeroZecommendCountRDD = recommendListRDD.reduceByKey(_+_)

      zeroZecommendCountRDD.foreach(println)

    } finally {
      sc.stop()
    }
  }

}
