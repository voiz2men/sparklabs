package com.skplanet.recopick.spark.driver.sample

import org.apache.spark.{SparkConf, SparkContext}

object ScalaGrammar {

  // http://jdm.kr/blog/85

  def main(args: Array[String]): Unit = {
    val sparkConf = new SparkConf().setAppName("My Spark Job1").setMaster("local")
    val sc = new SparkContext(sparkConf)

    // 1. 변수(Variables)
    //var x = 5
    val x = 5 // Good, constant
    //x = 6     // Bad
    var y: Double = 5 // explicit type

    // 2. 함수(Functions)
    def f(x: Int) = { x*x } // Good
    //def f(x: Int) { x*x }   // Bad

    // 2.2. 함수 선언2
    def f2(x: Any) = println(x) // Good
    //def f(x) = println(x)      // Bad

    // 2.3. 별칭 선언
    type R = Double

    // 2.4. 호출 타입 지정
    def f3(x: R) {} // call-by-value
    def f4(x: => R){} //call-by-name(lazy parameters)

    // 2.5. 익명 함수
    (x:R) => x*x // anonymous function
    ((x:Int) => println(x*x))(3) // 9

    // 2.6. 익명 함수2
    (1 to 5).map(_*2)
    (1 to 5).reduceLeft( _+_ )
    println((1 to 5).map(_*2)) // Vector(2, 4, 6, 8, 10)
    println((1 to 5).reduceLeft( _+_ )) // 15

    // 2.7. 익명 함수3
    (1 to 5).map( x => x*x )
    println((1 to 5).map( x => x*x )) // Vector(1, 4, 9, 16, 25)







    // Map
    /*
    val input = sc.parallelize(List(1, 2, 3, 4))
    val result = input.map(x => x * x)
    println(result.collect().mkString(", "))
    */

    val ints = List(1,2,3,4,5)

    // #1
    val strs = ints.map(_.toString) // <= strs is a List of Strings
    //strs.foreach(println)

    // #2
    val sqrs = ints.map { xxx => xxx * xxx } // => sqrs is a list of square values of ints
    sqrs.foreach(println)

    // flatMap
    // 1
    /*
    val input = sc.parallelize(List("abc", "a bc"))
    val result = input.flatMap(line => line.split(" "))
    result.foreach(println)
    */


    /*
    // 2
    val nestedNumbers = List(List(1, 2), List(3, 4), List(3, 5))
    var nestedNumbers2 = nestedNumbers.flatMap(x => x.map(_ * 2))
    println(nestedNumbers2)
    */




  }
}
